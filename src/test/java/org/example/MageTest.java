package org.example;

import org.example.heroes.Hero;
import org.example.heroes.Mage;
import org.example.items.Item;
import org.example.items.armors.Armor;
import org.example.items.armors.ArmorType;
import org.example.items.weapons.Weapon;
import org.example.items.weapons.WeaponType;
import org.example.shared.HeroAttributes;
import org.example.shared.InvalidArmorException;
import org.example.shared.InvalidWeaponException;
import org.example.shared.Slot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {


    public static Hero hero;

    public static String name;
    public static int startingHeroLevel;
    public static int startingHeroStrength;
    public static int startingHeroDexterity;
    public static int startingHeroIntelligence;
    public static int incrementOfHeroStrength;
    public static int incrementOfHeroDexterity;
    public static int incrementOfHeroIntelligence;
    public static HashMap<Slot, Item> emptyEquipment;


    public static String armorName;
    public static int armorRequiredLevel;
    public static int armorStrength;
    public static int armorDexterity;
    public static int armorIntelligence;
    public static Slot armorSlot;
    public static ArmorType armorType;


    public static String weaponName;
    public static int weaponRequiredLevel;
    public static int weaponDamage;
    public static Slot weaponSlot;
    public static WeaponType weaponType;


    @BeforeEach
    void setUp() {

        // gonna re-initailize the varaibles before every test runs
        name = "Saruman";
        startingHeroLevel = 1;
        startingHeroStrength = 1;
        startingHeroDexterity = 1;
        startingHeroIntelligence = 8;
        incrementOfHeroStrength = 1;
        incrementOfHeroDexterity = 1;
        incrementOfHeroIntelligence = 5;

        // set up empty equipment to compare to
        emptyEquipment = new HashMap<>();
        emptyEquipment.put(Slot.HEAD, null);
        emptyEquipment.put(Slot.BODY, null);
        emptyEquipment.put(Slot.LEGS, null);
        emptyEquipment.put(Slot.WEAPON, null);

    }


    // tests to check if a new hero was instantiated with correct fields
    @Test
    void create_newHero_checkIfNameIsCorrect() {
        // hero setup
        hero = new Mage(name);
        assertEquals(name, hero.getName());
    }

    @Test
    void create_newHero_checkIfLevelIsCorrect() {
        // hero setup
        hero = new Mage(name);
        assertEquals(startingHeroLevel, hero.getLevel());
    }

    @Test
    void create_newHero_checkIfStrengthIsCorrect() {
        // hero setup
        hero = new Mage(name);
        assertEquals(startingHeroStrength, hero.getLevelAttributes().getStrength());
    }

    @Test
    void create_newHero_checkIfDexterityIsCorrect() {
        // hero setup
        hero = new Mage(name);
        assertEquals(startingHeroDexterity, hero.getLevelAttributes().getDexterity());
    }

    @Test
    void create_newHero_checkIfIntelligenceIsCorrect() {
        // hero setup
        hero = new Mage(name);
        assertEquals(startingHeroIntelligence, hero.getLevelAttributes().getIntelligence());
    }

    @Test
    void create_newHero_checkIfEquipmentisEmpty() {
        // hero setup
        hero = new Mage(name);
        assertEquals(emptyEquipment, hero.getEquipment());
    }


    // test to check if hero is levelling up properly
    @Test
    void levelUpHero_heroLevel_shouldIncrease() {
        // level setup
        int expectedLevel = 11;
        // hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }

        assertEquals(expectedLevel, hero.getLevel());
    }

    @Test
    void levelUpHero_heroAttributeStrength_shouldIncrease() {
        // level setup
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength;
        // hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        assertEquals(expectedHeroStrengthAfterLevelUp, hero.getLevelAttributes().getStrength());
    }

    @Test
    void levelUpHero_heroAttributeDexterity_shouldIncrease() {
        // level setup
        int expectedLevel = 11;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity;
        // hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        assertEquals(expectedHeroDexterityAfterLevelUp, hero.getLevelAttributes().getDexterity());
    }

    @Test
    void levelUpHero_heroAttributeIntelligence_shouldIncrease() {
        // level setup
        int expectedLevel = 11;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence;
        // hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        assertEquals(expectedHeroIntelligenceAfterLevelUp, hero.getLevelAttributes().getIntelligence());
    }


    //    test for equipping the armor
    @Test
    void equip_allowedArmor_noExceptionThrown() {
        // setup of allowedArmor
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);

        //        hero setup
        hero = new Mage(name);

        assertDoesNotThrow(() -> {
            hero.equip(armor);
        });
    }

    @Test
    void equip_allowedArmor_updateEquipment() {
        // setup of allowed Armor
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);

        //        hero setup
        hero = new Mage(name);
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        // expected equipment setup
        HashMap<Slot, Item> expectedMap = emptyEquipment;
        expectedMap.put(Slot.BODY, armor);

        assertEquals(expectedMap, hero.getEquipment());
    }

    @Test
    void equip_notAllowedtypeOfArmor_exceptionThrown() {
        // setup of NOT allowed Armor type
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.LEATHER;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);

        //        hero setup
        hero = new Mage(name);
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            System.out.println("AS EXPECTED " + e);
        }
//        check if throws an error
        assertThrows(InvalidArmorException.class, () -> {
            hero.equip(armor);
        });
    }

    @Test
    void equip_notAllowedRequiredLevelOfArmor_ExceptionThrown() {
        // setup of NOT allowed Armor level
        armorName = "invisible cloak";
        armorRequiredLevel = Integer.MAX_VALUE;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);

        //        hero setup
        hero = new Mage(name);
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            System.out.println("AS EXPECTED " + e);
        }
//        check if throws an error
        assertThrows(InvalidArmorException.class, () -> {
            hero.equip(armor);
        });
    }


    //    tests for equipping the weapon
    @Test
    void equip_allowedWeapon_noExceptionThrown() {
        // allowed weapon setup
        weaponName = "midnight wand";
        weaponRequiredLevel = 1;
        weaponDamage = 10;
        weaponSlot = Slot.WEAPON;
        weaponType = WeaponType.WAND;
        Weapon weapon = new Weapon(weaponName, weaponRequiredLevel, weaponType, weaponDamage);
        //        hero setup
        hero = new Mage(name);

        assertDoesNotThrow(() -> {
            hero.equip(weapon);
        });
    }

    @Test
    void equip_allowedWeapon_updateEquipment() {
        // allowed weapon setup
        weaponName = "midnight wand";
        weaponRequiredLevel = 1;
        weaponDamage = 10;
        weaponSlot = Slot.WEAPON;
        weaponType = WeaponType.WAND;
        Weapon weapon = new Weapon(weaponName, weaponRequiredLevel, weaponType, weaponDamage);
        //        hero setup
        hero = new Mage(name);
        try {
            hero.equip(weapon);
        } catch (InvalidWeaponException e) {
            throw new RuntimeException(e);
        }

//        expected equipment setup
        HashMap<Slot, Item> expectedEquipment = emptyEquipment;
        expectedEquipment.put(Slot.WEAPON, weapon);

        assertEquals(expectedEquipment, hero.getEquipment());
    }

    @Test
    void equip_notAllowedtypeOfWeapon_exceptionThrown() {
        // NOT allowed TYPE weapon setup
        weaponName = "midnight sword";
        weaponRequiredLevel = 1;
        weaponDamage = 10;
        weaponSlot = Slot.WEAPON;
        weaponType = WeaponType.SWORD;
        Weapon weapon = new Weapon(weaponName, weaponRequiredLevel, weaponType, weaponDamage);
        //        hero setup
        hero = new Mage(name);
        try {
            hero.equip(weapon);
        } catch (InvalidWeaponException e) {
            System.out.println("AS EXPECTED " + e);
        }
//        check if throws an error
        assertThrows(InvalidWeaponException.class, () -> {
            hero.equip(weapon);
        });
    }

    @Test
    void equip_notAllowedRequiredLevelOfWeapon_ExceptionThrown() {
        // NOT allowed LEVEL of weapon setup
        weaponName = "midnight wand";
        weaponRequiredLevel = Integer.MAX_VALUE;
        weaponDamage = 10;
        weaponSlot = Slot.WEAPON;
        weaponType = WeaponType.WAND;
        Weapon weapon = new Weapon(weaponName, weaponRequiredLevel, weaponType, weaponDamage);
        //        hero setup
        hero = new Mage(name);
        try {
            hero.equip(weapon);
        } catch (InvalidWeaponException e) {
            System.out.println("AS EXPECTED " + e);
        }
//        check if throws an error
        assertThrows(InvalidWeaponException.class, () -> {
            hero.equip(weapon);
        });
    }


    // tests for total attributes
    @Test
    void count_StrengthFromTotalAttributesAtGivenLevel_withoutArmor() {
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        // hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        // hero attributes setup
        HeroAttributes actual = hero.totalAttributes();

        assertEquals(expected.getStrength(), actual.getStrength());
    }

    @Test
    void count_DexterityFromTotalAttributesAtGivenLevel_withoutArmor() {
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        // hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        // hero attributes setup
        HeroAttributes actual = hero.totalAttributes();

        assertEquals(expected.getDexterity(), actual.getDexterity());
    }

    @Test
    void count_IntelligenceFromTotalAttributesAtGivenLevel_withoutArmor() {
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        // hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        // hero attributes setup
        HeroAttributes actual = hero.totalAttributes();

        assertEquals(expected.getIntelligence(), actual.getIntelligence());
    }

    @Test
    void count_StrengthFromTotalAttributesAtGivenLevel_withOnePieceOfArmor() {
        // setup of allowed Armor
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = (startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength) + armorStrength;
        int expectedHeroDexterityAfterLevelUp = (startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity) + armorDexterity;
        int expectedHeroIntelligenceAfterLevelUp = (startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence) + armorIntelligence;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);

        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        // hero attributes setup
        HeroAttributes actual = hero.totalAttributes();

        assertEquals(expected.getStrength(), actual.getStrength());
    }

    @Test
    void count_DexterityFromTotalAttributesAtGivenLevel_withOnePieceOfArmor() {
        // setup of allowed Armor
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = (startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength) + armorStrength;
        int expectedHeroDexterityAfterLevelUp = (startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity) + armorDexterity;
        int expectedHeroIntelligenceAfterLevelUp = (startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence) + armorIntelligence;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);

        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        // hero attributes setup
        HeroAttributes actual = hero.totalAttributes();

        assertEquals(expected.getDexterity(), actual.getDexterity());
    }

    @Test
    void count_IntelligenceFromTotalAttributesAtGivenLevel_withOnePieceOfArmor() {
        // setup of allowed Armor
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = (startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength) + armorStrength;
        int expectedHeroDexterityAfterLevelUp = (startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity) + armorDexterity;
        int expectedHeroIntelligenceAfterLevelUp = (startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence) + armorIntelligence;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);

        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        // hero attributes setup
        HeroAttributes actual = hero.totalAttributes();

        assertEquals(expected.getIntelligence(), actual.getIntelligence());
    }

    @Test
    void count_StrengthFromTotalAttributesAtGivenLevel_withTwoPieceOfArmor() {
        // setup of allowed Armor
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);
        // setup of allowed Armor
        String armorName2 = "mighty hat";
        int armorRequiredLevel2 = 1;
        Slot armorSlot2 = Slot.HEAD;
        ArmorType armorType2 = ArmorType.CLOTH;
        int armorStrength2 = 2;
        int armorDexterity2 = 2;
        int armorIntelligence2 = 1;
        Armor armor2 = new Armor(armorName2, armorRequiredLevel2, armorSlot2, armorType2, armorStrength2, armorDexterity2, armorIntelligence2);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength + armorStrength + armorStrength2;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity + armorDexterity + armorDexterity2;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence + armorIntelligence + armorIntelligence2;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        try {
            hero.equip(armor2);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        // hero attributes setup
        HeroAttributes actual = hero.totalAttributes();

        assertEquals(expected.getStrength(), actual.getStrength());
    }


    @Test
    void count_DexterityFromTotalAttributesAtGivenLevel_withTwoPieceOfArmor() {
        // setup of allowed Armor
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);
        // setup of allowed Armor
        String armorName2 = "mighty hat";
        int armorRequiredLevel2 = 1;
        Slot armorSlot2 = Slot.HEAD;
        ArmorType armorType2 = ArmorType.CLOTH;
        int armorStrength2 = 2;
        int armorDexterity2 = 2;
        int armorIntelligence2 = 1;
        Armor armor2 = new Armor(armorName2, armorRequiredLevel2, armorSlot2, armorType2, armorStrength2, armorDexterity2, armorIntelligence2);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength + armorStrength + armorStrength2;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity + armorDexterity + armorDexterity2;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence + armorIntelligence + armorIntelligence2;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        try {
            hero.equip(armor2);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        // hero attributes setup
        HeroAttributes actual = hero.totalAttributes();

        assertEquals(expected.getDexterity(), actual.getDexterity());
    }

    @Test
    void count_IntelligenceFromTotalAttributesAtGivenLevel_withTwoPieceOfArmor() {
        // setup of allowed Armor
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);
        // setup of allowed Armor
        String armorName2 = "mighty hat";
        int armorRequiredLevel2 = 1;
        Slot armorSlot2 = Slot.HEAD;
        ArmorType armorType2 = ArmorType.CLOTH;
        int armorStrength2 = 2;
        int armorDexterity2 = 2;
        int armorIntelligence2 = 1;
        Armor armor2 = new Armor(armorName2, armorRequiredLevel2, armorSlot2, armorType2, armorStrength2, armorDexterity2, armorIntelligence2);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength + armorStrength + armorStrength2;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity + armorDexterity + armorDexterity2;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence + armorIntelligence + armorIntelligence2;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        try {
            hero.equip(armor2);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        // hero attributes setup
        HeroAttributes actual = hero.totalAttributes();

        assertEquals(expected.getIntelligence(), actual.getIntelligence());
    }


    @Test
    void count_StrengthFromTotalAttributesAtGivenLevel_withReplacedPieceOfArmor() {
        // setup of allowed Armor
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);
        // setup of allowed Armor
        String armorName2 = "cloth robe";
        int armorRequiredLevel2 = 1;
        Slot armorSlot2 = Slot.BODY;
        ArmorType armorType2 = ArmorType.CLOTH;
        int armorStrength2 = 2;
        int armorDexterity2 = 2;
        int armorIntelligence2 = 1;
        Armor armor2 = new Armor(armorName2, armorRequiredLevel2, armorSlot2, armorType2, armorStrength2, armorDexterity2, armorIntelligence2);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength + armorStrength2;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity + armorDexterity2;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence + armorIntelligence2;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        try {
            hero.equip(armor2);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        // hero attributes setup
        HeroAttributes actual = hero.totalAttributes();

        assertEquals(expected.getStrength(), actual.getStrength());
    }


    @Test
    void count_DexterityFromTotalAttributesAtGivenLevel_withReplacedPieceOfArmor() {
        // setup of allowed Armor
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);
        // setup of allowed Armor
        String armorName2 = "cloth robe";
        int armorRequiredLevel2 = 1;
        Slot armorSlot2 = Slot.BODY;
        ArmorType armorType2 = ArmorType.CLOTH;
        int armorStrength2 = 2;
        int armorDexterity2 = 2;
        int armorIntelligence2 = 1;
        Armor armor2 = new Armor(armorName2, armorRequiredLevel2, armorSlot2, armorType2, armorStrength2, armorDexterity2, armorIntelligence2);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength + armorStrength2;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity + armorDexterity2;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence + armorIntelligence2;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        try {
            hero.equip(armor2);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        // hero attributes setup
        HeroAttributes actual = hero.totalAttributes();

        assertEquals(expected.getDexterity(), actual.getDexterity());
    }

    @Test
    void count_IntelligenceFromTotalAttributesAtGivenLevel_withReplacedPieceOfArmor() {
        // setup of allowed Armor
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);
        // setup of allowed Armor
        String armorName2 = "cloth robe";
        int armorRequiredLevel2 = 1;
        Slot armorSlot2 = Slot.BODY;
        ArmorType armorType2 = ArmorType.CLOTH;
        int armorStrength2 = 2;
        int armorDexterity2 = 2;
        int armorIntelligence2 = 1;
        Armor armor2 = new Armor(armorName2, armorRequiredLevel2, armorSlot2, armorType2, armorStrength2, armorDexterity2, armorIntelligence2);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength + armorStrength2;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity + armorDexterity2;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence + armorIntelligence2;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        try {
            hero.equip(armor2);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        // hero attributes setup
        HeroAttributes actual = hero.totalAttributes();

        assertEquals(expected.getIntelligence(), actual.getIntelligence());
    }


    @Test
    void count_totalDamage_noWeaponNoArmor() {
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength ;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity ;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        double expectedDamage = 1 * (1 + expectedHeroIntelligenceAfterLevelUp / 100.0);

        assertEquals(expectedDamage, hero.damage());
    }

    @Test
    void count_totalDamage_withWeaponNoArmor() {
        weaponName = "midnight wand";
        weaponRequiredLevel = 1;
        weaponDamage = 10;
        weaponSlot = Slot.WEAPON;
        weaponType = WeaponType.WAND;
        Weapon weapon = new Weapon(weaponName, weaponRequiredLevel, weaponType, weaponDamage);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength ;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity ;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(weapon);
        } catch (InvalidWeaponException e) {
            throw new RuntimeException(e);
        }
        double expectedDamage = weaponDamage * (1 + expectedHeroIntelligenceAfterLevelUp / 100.0);

        assertEquals(expectedDamage, hero.damage());
    }

    @Test
    void count_totalDamage_withWeaponwithArmor() {
        armorName = "invisible cloak";
        armorRequiredLevel = 1;
        armorSlot = Slot.BODY;
        armorType = ArmorType.CLOTH;
        armorStrength = 1;
        armorDexterity = 2;
        armorIntelligence = 3;
        Armor armor = new Armor(armorName, armorRequiredLevel, armorSlot, armorType, armorStrength, armorDexterity, armorIntelligence);
        weaponName = "midnight wand";
        weaponRequiredLevel = 1;
        weaponDamage = 10;
        weaponSlot = Slot.WEAPON;
        weaponType = WeaponType.WAND;
        Weapon weapon = new Weapon(weaponName, weaponRequiredLevel, weaponType, weaponDamage);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength +armorStrength;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity +armorDexterity;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence +armorIntelligence;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(weapon);
        } catch (InvalidWeaponException e) {
            throw new RuntimeException(e);
        }
        try {
            hero.equip(armor);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }
        double expectedDamage = weaponDamage * (1 + expectedHeroIntelligenceAfterLevelUp / 100.0);

        assertEquals(expectedDamage, hero.damage());
    }
    @Test
    void count_totalDamage_withReplacedWeaponNoArmor() {
        // weapon setup
        weaponName = "midnight wand";
        weaponRequiredLevel = 1;
        weaponDamage = 10;
        weaponSlot = Slot.WEAPON;
        weaponType = WeaponType.WAND;
        Weapon weapon = new Weapon(weaponName, weaponRequiredLevel, weaponType, weaponDamage);
        String weaponName2 = "wooden wand";
        int weaponRequiredLevel2 = 1;
        int weaponDamage2 = 2;
        Slot weaponSlot2 = Slot.WEAPON;
        WeaponType weaponType2 = WeaponType.WAND;
        Weapon weapon2 = new Weapon(weaponName2, weaponRequiredLevel2, weaponType2, weaponDamage2);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength ;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity ;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(weapon);
        } catch (InvalidWeaponException e) {
            throw new RuntimeException(e);
        }
        try {
            hero.equip(weapon2);
        } catch (InvalidWeaponException e) {
            throw new RuntimeException(e);
        }

        double expectedDamage = weaponDamage2 * (1 + expectedHeroIntelligenceAfterLevelUp / 100.0);

        assertEquals(expectedDamage, hero.damage());
    }
    @Test
    void display_hero_asString() {
        // weapon setup
        weaponName = "midnight wand";
        weaponRequiredLevel = 1;
        weaponDamage = 10;
        weaponSlot = Slot.WEAPON;
        weaponType = WeaponType.WAND;
        Weapon weapon = new Weapon(weaponName, weaponRequiredLevel, weaponType, weaponDamage);
        // expected hero attributes
        int expectedLevel = 11;
        int expectedHeroStrengthAfterLevelUp = startingHeroStrength + (expectedLevel - 1) * incrementOfHeroStrength ;
        int expectedHeroDexterityAfterLevelUp = startingHeroDexterity + (expectedLevel - 1) * incrementOfHeroDexterity ;
        int expectedHeroIntelligenceAfterLevelUp = startingHeroIntelligence + (expectedLevel - 1) * incrementOfHeroIntelligence;
        HeroAttributes expected = new HeroAttributes(expectedHeroStrengthAfterLevelUp, expectedHeroDexterityAfterLevelUp, expectedHeroIntelligenceAfterLevelUp);
        //        hero setup
        hero = new Mage(name);
        for (int i = 1; i < expectedLevel; i++) {
            hero.levelUp();
        }
        try {
            hero.equip(weapon);
        } catch (InvalidWeaponException e) {
            throw new RuntimeException(e);
        }
        double expectedDamage = weaponDamage * (1 + expectedHeroIntelligenceAfterLevelUp / 100.0);

        String displayHero = String.format("===== HERO ====\n" +
                        "Name: %s\n" +
                        "Class: %s\n" +
                        "Level: %d\n" +
                        "Total strength: %d\n" +
                        "Total dexterity: %d\n" +
                        "Total intelligence: %d\n" +
                        "Damage: %.2f\n" +
                        "==============",
                name,
                "Mage",
                expectedLevel,
                expectedHeroStrengthAfterLevelUp,
                expectedHeroDexterityAfterLevelUp,
                expectedHeroIntelligenceAfterLevelUp,
                expectedDamage);

        assertEquals(displayHero, hero.display());
    }

}