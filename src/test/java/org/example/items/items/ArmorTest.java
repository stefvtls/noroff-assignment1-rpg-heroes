package org.example.items.items;

import org.example.items.armors.Armor;
import org.example.items.armors.ArmorType;
import org.example.shared.Slot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    public static int requiredLevel;
    public static int strength;
    public static int dexterity;
    public static int intelligence;
    public static String name;
    public static Slot slot;
    public static ArmorType type;


    @BeforeEach
    void setUp() {
        name = "invisible cloak";
        requiredLevel = 10;
        strength = 1;
        dexterity = 2;
        intelligence = 3;
        slot = Slot.BODY;
        type = ArmorType.LEATHER;
    }


    @Test
    void create_newArmor_checkName(){
        Armor actual = new Armor(name, requiredLevel, slot, type, strength,dexterity,intelligence);
        assertEquals(name, actual.getName());

    }
    @Test
    void create_newArmor_checkSlot(){
        Armor actual = new Armor(name, requiredLevel, slot, type, strength,dexterity,intelligence);
        assertEquals(slot, actual.getSlot());
    }
    @Test
    void create_newArmor_checkLevel (){
        Armor actual = new Armor(name, requiredLevel, slot, type, strength,dexterity,intelligence);
        assertEquals(requiredLevel, actual.getRequiredLevel());
    }
    @Test
    void create_newArmor_checkType (){
        Armor actual = new Armor(name, requiredLevel, slot, type, strength,dexterity,intelligence);
        assertEquals(type, actual.getType());

    }
    @Test
    void create_newArmor_checkStrength (){
        Armor actual = new Armor(name, requiredLevel, slot, type, strength,dexterity,intelligence);
        assertEquals(strength, actual.getArmorAttributes().getStrength());
    }
    @Test
    void create_newArmor_checkDexterity (){
        Armor actual = new Armor(name, requiredLevel, slot, type, strength,dexterity,intelligence);
        assertEquals(dexterity, actual.getArmorAttributes().getDexterity());
    }

    @Test
    void create_newArmor_checkIntelligence (){
        Armor actual = new Armor(name, requiredLevel, slot, type, strength,dexterity,intelligence);
        assertEquals(intelligence, actual.getArmorAttributes().getIntelligence());
    }


    @Test
    void create_newArmor_checkMessage (){
        String message = "" +
                "=============================\n" +
                "Name: " + name + "\n" +
                "Slot: " + slot + "\n" +
                "Attribute of strength: " + strength + "\n" +
                "Attribute of dexterity: " + dexterity + "\n" +
                "Attribute of intelligence: " + intelligence + "\n" +
                "Required level: " + requiredLevel + "\n" +
                "=============================\n";
        Armor actual = new Armor(name, requiredLevel, slot, type, strength,dexterity,intelligence);
        assertEquals(message, actual.displayItem());
    }

}