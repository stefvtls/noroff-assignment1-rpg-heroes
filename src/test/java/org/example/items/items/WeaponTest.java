package org.example.items.items;

import org.example.items.weapons.Weapon;
import org.example.items.weapons.WeaponType;
import org.example.items.Item;
import org.example.shared.Slot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class WeaponTest {
    public static int requiredLevel;
    public static String name;
    public static int weaponDamage;
    public static Slot slot;
    public static WeaponType type;


    @BeforeEach
    void setUp() {
        name = "mighty dagger";
        requiredLevel = 3;
        weaponDamage = 10;
        type = WeaponType.DAGGER;
        slot = Slot.WEAPON;
    }


    @Test
    void create_newArmor_checkName() {
        Weapon actual = new Weapon(name, requiredLevel, type, weaponDamage);
        assertEquals(name, actual.getName());

    }

    @Test
    void create_newArmor_checkSlot() {
        Weapon actual = new Weapon(name, requiredLevel, type, weaponDamage);
        assertEquals(slot, actual.getSlot());
    }

    @Test
    void create_newArmor_checkLevel() {
        Weapon actual = new Weapon(name, requiredLevel, type, weaponDamage);
        assertEquals(requiredLevel, actual.getRequiredLevel());
    }

    @Test
    void create_newArmor_checkType() {
        Weapon actual = new Weapon(name, requiredLevel, type, weaponDamage);
        assertEquals(type, actual.getType());

    }

    @Test
    void create_newArmor_checkDamage() {
        Weapon actual = new Weapon(name, requiredLevel, type, weaponDamage);
        assertEquals(weaponDamage, actual.getWeaponDamage());

    }

    @Test
    void create_newArmor_checkMessage() {
        String message = "=============================\n" +
                "Name: mighty dagger\n" +
                "Type: DAGGER\n" +
                "Slot: WEAPON\n" +
                "Damage: 10\n" +
                "Required level: 3\n" +
                "=============================\n";
        Weapon actual = new Weapon(name, requiredLevel, type, weaponDamage);
        assertEquals(message, actual.displayItem());
    }


}

