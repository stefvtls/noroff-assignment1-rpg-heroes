package org.example.shared;

public enum Slot
    // enum of available slots
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON;
    }

