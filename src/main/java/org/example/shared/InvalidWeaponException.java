package org.example.shared;

public class InvalidWeaponException extends Exception{
    // custom exception created when trying to equip a weapon to a hero when:
    // hero has lower level than the requirements of the weapon
    // hero cannot equip this particular type of weapon
    public InvalidWeaponException(String message) {
        super(message);
    }
}