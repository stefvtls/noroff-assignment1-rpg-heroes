package org.example.shared;

import java.util.List;

public class HeroAttributes {

// This class represents the hero attributes, such as strength, dexterity, and intelligence.
// A constructor sets the initial values for these attributes. HeroAttributes Class is used as Hero attribute and an Armor attribute.

    private int strength;
    private int dexterity;
    private int intelligence;



    //    constructor
    public HeroAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;

    }

    //  getters and setters
    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }




    // Two overloaded methods for modifying the attributes.
    public HeroAttributes modifyAttributes(HeroAttributes... attributesObjects) {                                // Takes an unlimited number of HeroAttributes objects as arguments and adds the values of their attributes together.
                                                                                                                // creating empty HeroAttribute object, which will get hold of total sum of all attributes
        HeroAttributes resultHeroAttributesObject = new HeroAttributes(0,0,0);
        for (HeroAttributes object: attributesObjects) {                                                                            // looping through all the arguments of our method and...
            resultHeroAttributesObject.setStrength(resultHeroAttributesObject.getStrength() + object.getStrength());                // ...getting hold  of their strength and storing it...
            resultHeroAttributesObject.setDexterity(resultHeroAttributesObject.getDexterity() + object.getDexterity());             // ...getting hold  of their dexterity and storing it...
            resultHeroAttributesObject.setIntelligence(resultHeroAttributesObject.getIntelligence() + object.getIntelligence());    // ...getting hold  of their intelligence and storing it.
        }
        return resultHeroAttributesObject;                                                                      // returning an object holding a sum of all attributes
    }
    public HeroAttributes modifyAttributes(List<HeroAttributes> attributesList) {                               // Takes a List of HeroAttributes objects as an argument and adds the values of their attributes together in the same way.
                                                                                                                // creating empty HeroAttribute object, which will get hold of total sum of all attributes
        HeroAttributes resultHeroAttributesObject = new HeroAttributes(0,0,0);
//        looping through all the objects stored in the argument list
        for (HeroAttributes object: attributesList) {                                                                               // looping through all the HeroAttributes objects stored in a list and...
            resultHeroAttributesObject.setStrength(resultHeroAttributesObject.getStrength() + object.getStrength());                // ...getting hold  of their strength and storing it...
            resultHeroAttributesObject.setDexterity(resultHeroAttributesObject.getDexterity() + object.getDexterity());             // ...getting hold  of their dexterity and storing it...
            resultHeroAttributesObject.setIntelligence(resultHeroAttributesObject.getIntelligence() + object.getIntelligence());    // ...getting hold  of their intelligence and storing it.
        }
        return resultHeroAttributesObject;                                                                      // returning an object holding a sum of all attributes
    }


}
