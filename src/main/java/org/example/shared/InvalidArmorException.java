package org.example.shared;

public class InvalidArmorException extends Exception{
    public InvalidArmorException(String message) {
        // custom exception created when trying to equip aan armor to a hero when:
        // hero has lower level than the requirements of the armor
        // hero cannot equip this particular type of armor
        super(message);
    }
}
