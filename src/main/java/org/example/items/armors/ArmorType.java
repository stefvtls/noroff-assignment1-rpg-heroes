package org.example.items.armors;

public enum ArmorType {
    // enum of armor types
    CLOTH,
    LEATHER,
    MAIL,
    PLATE;
}
