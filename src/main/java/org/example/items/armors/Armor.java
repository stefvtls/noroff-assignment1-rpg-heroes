package org.example.items.armors;

import org.example.shared.HeroAttributes;
import org.example.shared.Slot;
import org.example.items.Item;

public class Armor extends Item {
    // Armor is a child of the Item class.
    // Every piece of armor has a specific type - some types of armor can only be held by specific heroes.
    // Every piece of armor also has a set of attributes,
    // which contributes to the total attributes of hero and in consequences also impacts the amount damage of hero.

    private HeroAttributes armorAttributes;
    private ArmorType type;


    public Armor(String name, int requiredLevel, Slot slot, ArmorType type, int strength, int dexterity, int intelligence) {
        super(name, requiredLevel, slot);
        this.type = type;
        this.armorAttributes = new HeroAttributes(strength, dexterity, intelligence);
    }


    // getters
    public ArmorType getType() {
        return type;
    }

    public HeroAttributes getArmorAttributes() {
        return armorAttributes;
    }


    // method to display important features of this particular armor
    @Override
    public String displayItem() {
        String message = "" +
                "=============================\n" +
                "Name: " + this.name + "\n" +
                "Slot: " + this.slot + "\n" +
                "Attribute of strength: " + this.armorAttributes.getStrength() + "\n" +             // displays strength boost of this piece of armor
                "Attribute of dexterity: " + this.armorAttributes.getDexterity() + "\n" +           // displays dexterity boost of this piece of armor
                "Attribute of intelligence: " + this.armorAttributes.getIntelligence() + "\n" +     // displays intelligence boost of this piece of armor
                "Required level: " + this.requiredLevel + "\n" +
                "=============================\n";
        System.out.println(message);
        return message;
    }
}
