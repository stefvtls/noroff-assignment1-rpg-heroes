package org.example.items;

import org.example.shared.Slot;

public abstract class Item {
//    The Item class is the abstract parent class for child weapon and armor class.
//    It contains the common properties and methods shared by all items.
//    Each item has a name, required level, and a slot in hero's equipment which they can reside in.


    protected String name;
    protected int requiredLevel;
    protected Slot slot;

    // constructor
    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }


    // getters
    public String getName() {
        return name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }


    // abstract method of displaying important features of every item. varies per child class.
    public abstract String displayItem();
}
