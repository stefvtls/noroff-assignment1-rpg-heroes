package org.example.items.weapons;

public enum WeaponType {
    // enum of weapon types
    AXE,
    BOW,
    DAGGER,
    HAMMER,
    STAFF,
    SWORD,
    WAND;
}
