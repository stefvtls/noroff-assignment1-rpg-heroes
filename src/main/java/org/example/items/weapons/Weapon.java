package org.example.items.weapons;

import org.example.shared.Slot;
import org.example.items.Item;

public class Weapon extends Item {
    // Weapon is a child of the Item class.
    // Every weapon has a specific type - some types of weapons can only be held by specific hero types.
    // Every weapon has a specific damage factor, which increase hero damage
    // weapons can only reside in Slot.WEAPON slot of hero's equipment
    private static final Slot slotGenerator = Slot.WEAPON;
    private WeaponType type;
    private int weaponDamage;

    public Weapon(String name, int requiredLevel, WeaponType type, int weaponDamage) {
        super(name, requiredLevel, slotGenerator);
        this.type = type;
        this.weaponDamage = weaponDamage;
    }
    // getters
    public WeaponType getType() {
        return type;
    }

    public int getWeaponDamage() {
        return weaponDamage;
    }
    // method to display important features of this particular armor
    @Override
    public String displayItem() {
        String message = "" +
                "=============================\n" +
                "Name: " + this.name + "\n" +
                "Type: " + this.type + "\n" +
                "Slot: " + this.slot + "\n" +
                "Damage: " + this.weaponDamage + "\n" +
                "Required level: " + this.requiredLevel + "\n" +
                "=============================\n";
        System.out.println(message);
        return message;
    }

}
