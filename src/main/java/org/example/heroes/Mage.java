package org.example.heroes;

import org.example.items.armors.ArmorType;
import org.example.items.weapons.Weapon;
import org.example.items.weapons.WeaponType;
import org.example.shared.HeroAttributes;
import org.example.shared.Slot;

import java.util.List;


public class Mage extends Hero {
//    this is a child class of the Hero class.
//    a Mage can only equip cloth armor
//    a Mage can equip only staff or wand weapons.

    private static final int startingStrength = 1;
    private static final int startingDexterity = 1;
    private static final int startingIntelligence = 8;
    private static final int levellingStrength = 1;
    private static final int levellingDexterity = 1;
    private static final int levellingIntelligence = 5;
    private static final List<WeaponType> allowedWeapons = List.of(WeaponType.STAFF, WeaponType.WAND);
    private static final List<ArmorType> allowedArmors = List.of(ArmorType.CLOTH);


    // create a hero with starting attributes of strength, dexterity, intelligence and a lists of allowed armor and weapon types
    public Mage(String name) {
        super(name, startingStrength, startingDexterity, startingIntelligence, allowedWeapons, allowedArmors);
    }

    @Override
    public void levelUp() {
        super.levelUp();                  // hero levels up 1 level at a time
        this.levelAttributes =           // increase hero attributes when levelling up - use modifyAttributes method from HeroAttributes class to add increasing attributes to old hero attributes and store it.
                this.levelAttributes.modifyAttributes(this.levelAttributes, new HeroAttributes(levellingStrength, levellingDexterity, levellingIntelligence));
    }

    @Override
    public double damage() {                                                                // Calculate damage is based on the weapon equipped, if any, and the Mage's intelligence attribute.
        int weaponDamage;                                                                   // declare a variable that will be holding value of the weapon damage depending on if the weapon exist or not
        try {
            weaponDamage = ((Weapon) this.equipment.get(Slot.WEAPON)).getWeaponDamage();    // try to get the value of the weapon damage if it exist in the equipment
        } catch (Exception e) {
            System.out.println("hero not equipped with any weapon. fighting bare-handed."); // inform player, that hero fights bare-handed
            weaponDamage = 1;                                                               // if the weapon does not exist, the value of weapon damage is 1
        }
        return weaponDamage * (1 + totalAttributes().getIntelligence() / 100.0);            // actual damage: take into account only dominant attribute (dominant attribute varies per hero-child class). of hero dominant attribute and
    }
}

