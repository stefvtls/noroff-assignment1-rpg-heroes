package org.example.heroes;

import org.example.items.armors.Armor;
import org.example.items.armors.ArmorType;
import org.example.items.weapons.Weapon;
import org.example.items.weapons.WeaponType;
import org.example.shared.HeroAttributes;
import org.example.shared.InvalidArmorException;
import org.example.shared.InvalidWeaponException;
import org.example.shared.Slot;
import org.example.items.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Hero {
//    The Hero class is the abstract parent class for all hero types.
//    It contains the common properties and methods shared by all heroes.
//    Each hero has a name, level, and a set of attributes that are determined by their level.
//    The hero also has a list of valid weapon types and valid armor types that they can equip.
    private String name;
    protected int level = 1;
    protected HeroAttributes levelAttributes;

    protected List<WeaponType> validWeaponTypes;
    protected List<ArmorType> validArmorTypes;
    protected Map<Slot, Item> equipment = new HashMap<>();


    // Constructor for a new hero with the given name, strength, dexterity, intelligence, valid weapon types for a hero type, and valid armor types for a hero type.
    // It also initializes the equipment map with empty "null" values for every pre-determined slot.
    public Hero(String name, int strength, int dexterity, int intelligence, List<WeaponType> validWeaponTypes, List<ArmorType> validArmorTypes) {
        this.name = name;
        this.levelAttributes = new HeroAttributes(strength, dexterity, intelligence);
        this.validWeaponTypes = validWeaponTypes;
        this.validArmorTypes = validArmorTypes;
        for (Slot slot : Slot.values()) {
            equipment.put(slot, null);
        }
    }

    // getters
    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public HeroAttributes getLevelAttributes() {
        return levelAttributes;
    }

    public Map<Slot, Item> getEquipment() {
        return equipment;
    }


    // methods


    public void levelUp(){
        this.level += 1;                 // every hero levels up 1 level at a time, but gets different attributes when levelling up
    }                                   //  implementation of process varies per child class - method will be overridden in the child class


    //    two overloaded equip methods which takes in a Weapon or Armor object and adds it to the hero's equipment map.
    //    this methods will check if hero can equip this kind of item and if his level is high enough to equip it.
    public void equip(Weapon weapon) throws InvalidWeaponException {                                                // overloading equip methods - used when Weapon object is passed as an argument
        if (weapon.getRequiredLevel() > this.level || !this.validWeaponTypes.contains(weapon.getType())) {          // check if item which hero is trying to equip with is breaching Hero's possibilities to carry the item
            throw new InvalidWeaponException("Invalid Weapon: ");                                                   // if so - do not allow to equip the item which is not suitable for a hero and throw an exception
        }
        this.equipment.put(weapon.getSlot(), weapon);                                                               // add weapon to hero's equipment

    }

    public void equip(Armor armor) throws InvalidArmorException {                                                   // overloading equip methods - used when Armor object is passed as an argument
        if (armor.getRequiredLevel() > this.level || !this.validArmorTypes.contains(armor.getType())) {             // check if item which hero is trying to equip with is breaching Hero's possibilities to carry the item
            throw new InvalidArmorException("Invalid Armor: ");                                                     // if so - do not allow to equip the item which is not suitable for a hero and throw an exception
        }
        this.equipment.put(armor.getSlot(), armor);                                                                 // add armor to hero's equipment
    }


    public HeroAttributes totalAttributes() {                                               // method that calculates the total attributes of a hero based on their actual attributes and armor attributes that they are equipped with
        List<HeroAttributes> listOfHeroAttributesArguments = new ArrayList<>();             // creating a list of HeroAttributes objects which will be used as argument for the calculateAttributes function from the HeroAttribute class
        listOfHeroAttributesArguments.add(this.levelAttributes);                            // add attributes of this particular hero to list of future arguments
        for (Map.Entry<Slot, Item> entry : this.equipment.entrySet()) {                     // loop through all the equipment of a hero
            Item item = entry.getValue();                                                   // get hold of the item that is stored in the equipment
            if (item instanceof Armor) {                                                    // if item in the equipment exist, and it is not a weapon
                listOfHeroAttributesArguments.add(((Armor) item).getArmorAttributes());     // add armor attributes to the list of arguments
            }
        }
        // finally use the list of hero attributes objects as an argument for the modifyAttributes function
        // and return a HeroAttribute object holding value of all the respective fields of all the objects passed in as arguments
        return this.levelAttributes.modifyAttributes(listOfHeroAttributesArguments);
    }

    public abstract double damage();    // abstract class of calculating hero damage on the fly. Each child class have a different implementation of counting damage

    public String display() {                                               // method to create a String to display all important information about the hero
        HeroAttributes currentTotalAttributes = totalAttributes();
        String display = String.format("===== HERO ====\n" +
                        "Name: %s\n" +                                      // display String of hero's name
                        "Class: %s\n" +                                     // display String of hero's class
                        "Level: %d\n" +                                     // display int of hero's level
                        "Total strength: %d\n" +                            // display int of hero's strength
                        "Total dexterity: %d\n" +                           // display int of hero's dexterity
                        "Total intelligence: %d\n" +                        // display int of hero's intelligence
                        "Damage: %.2f\n" +                                  // display the damage formatted to two decimal places
                        "==============",
                this.name,
                this.getClass().getSimpleName(),                            // display the name of the class without the package name
                this.level,
                currentTotalAttributes.getStrength(),
                currentTotalAttributes.getDexterity(),
                currentTotalAttributes.getIntelligence(),
                this.damage());
        System.out.println(display);
        return display;
    }
}
