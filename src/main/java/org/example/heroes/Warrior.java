package org.example.heroes;

import org.example.items.armors.ArmorType;
import org.example.items.weapons.Weapon;
import org.example.items.weapons.WeaponType;
import org.example.shared.HeroAttributes;
import org.example.shared.Slot;


import java.util.List;

public class Warrior extends Hero {
    //    this is a child class of the Hero class.
    //    a Warrior can only equip plate or mail armor.
    //    a Warrior can equip axe, hammer and sword as weapons.
    private static final int startingStrength = 5;
    private static final int startingDexterity = 2;
    private static final int startingIntelligence = 1;
    private static final int levellingStrength = 3;
    private static final int levellingDexterity = 2;
    private static final int levellingIntelligence = 1;
    private static final List<WeaponType> allowedWeapons = List.of(WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD);
    private static final List<ArmorType> allowedArmors = List.of(ArmorType.MAIL, ArmorType.PLATE);



    // create a hero with starting attributes of strength, dexterity, intelligence and a lists of allowed armor and weapon types
    public Warrior(String name) {
        super(name, startingStrength, startingDexterity, startingIntelligence, allowedWeapons, allowedArmors);
    }


    @Override
    public void levelUp() {
        super.levelUp();                  // hero levels up 1 level at a time
        this.levelAttributes =           // increase hero attributes when levelling up - use modifyAttributes method from HeroAttributes class to add increasing attributes to old hero attributes and store it.
                this.levelAttributes.modifyAttributes(this.levelAttributes, new HeroAttributes(levellingStrength, levellingDexterity, levellingIntelligence));
    }

    @Override
    public double damage() {                                                                // calculate damage is based on the weapon equipped, if any, and the Warrior's intelligence attribute.
        int weaponDamage;                                                                   // declare a variable that will be holding value of the weapon damage depending on if the weapon exist or not
        try {
            weaponDamage = ((Weapon) this.equipment.get(Slot.WEAPON)).getWeaponDamage();    // try to get the value of the weapon damage if it exist in the equipment
        } catch (Exception e) {
            System.out.println("hero not equipped with any weapon. fighting bare-handed."); // inform player, that hero fights bare-handed
            weaponDamage = 1;                                                               // if the weapon does not exist, the value of weapon damage is 1
        }
        return weaponDamage * (1 + totalAttributes().getStrength() / 100.0);                // actual damage: take into account only dominant attribute (dominant attribute varies per hero-child class). of hero dominant attribute and
    }


}
