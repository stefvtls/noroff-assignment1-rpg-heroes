## RPG Heroes

This project is a console application built in Java that allows players to create and customize their own RPG heroes and weapons/pieces of armor.
The project follows the guidelines provided in the Noroff assignment and includes the minimum requirements as described in the Appendix A-C.




## Getting Started

Clone the repository: 
```git clone https://gitlab.com/stefvtls/noroff-assignment1-rpg-heroes.git```

This project uses Maven Wrapper, so you don't need to have Maven installed on your machine. 
The wrapper script will automatically download the version of Maven that is specified in the pom.xml file.

To install run:
`./mvnw clean install`

To run tests run
`./mvnw test`

Alternatively:
Open the project in IntelliJ Ultimate with JDK 17.




## Built With
• IntelliJ IDEA with JDK 17.

• Junit 5.9.2

• Maven




## Author contact
stefvtls@gmail.com



## License
This project is licensed under the MIT License.




## Acknowledgments
Special thanks to the Noroff for providing the opportunity to develop this project.




## Note
This is a student project and it should not be used as a production-ready software.